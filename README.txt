# Upload Image Changer

Allows change an original image while upload, by applying a selected Image Style.

## Installation
Install as normal (see http://drupal.org/documentation/install/modules-themes).

## Using
1. Enable the module.
2. Prepare an Image Style that will be used within Upload Image Changer.
3. Go to an Image field's settings and select the Image Style. Save settings.

Now while an image of appropriate type will be uploaded, will be applied the selected style.

## Requirements
This module has dependency on Drupal core's Image and Image Style modules.
