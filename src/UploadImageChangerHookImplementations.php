<?php

namespace Drupal\upload_image_changer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\field\Entity\FieldConfig;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Hook implementations for the Image Upload module.
 */
class UploadImageChangerHookImplementations {

  use StringTranslationTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new ImageAPIOptimizeHookImplementations object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The stream translation service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity Field Manager service.
   * @param \Drupal\Core\Image\ImageFactory|null $image_factory
   *   Image Factory service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system service.
   */
  public function __construct(TranslationInterface $string_translation, EntityFieldManagerInterface $entity_field_manager, ImageFactory $image_factory = NULL, FileSystemInterface $file_system) {
    $this->stringTranslation = $string_translation;
    $this->entityFieldManager = $entity_field_manager;
    $this->imageFactory = $image_factory;
    $this->fileSystem = $file_system;
  }

  /**
   * Implements hook_field_widget_WIDGET_ID_form_alter().
   */
  public function fieldWidgetImageImageFormAlter(&$element, FormStateInterface $form_state, $context) {
    /** @var \Drupal\field\Entity\FieldConfig $field_definition */
    $field_definition = $context['items']->getFieldDefinition();
    $field_name = $field_definition->getName();
    $entity_type = $field_definition->getTargetEntityTypeId();
    $bundle = $field_definition->getTargetBundle();

    $definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    if (isset($definitions[$field_name])) {
      $upload_image_style = $definitions[$field_name]->getThirdPartySetting('upload_image_changer', 'upload_image_style');
    }

    if (isset($upload_image_style)) {
      // Prepare the upload image change validator callback.
      $upload_image_validator = ['_upload_image_changer_style_on_upload_callback' => [
        $upload_image_style,
      ]];
      // And set it, to be executed firstly.
      $element['#upload_validators'] = $upload_image_validator + $element['#upload_validators'];
    }
  }

  /**
   * Apply image style on image upload in validator.
   *
   * @param \Drupal\file\FileInterface $file
   *   File object.
   * @param string $image_style_name
   *   Image style name.
   */
  public function applyImageStyleOnUploadValidatorCallback(FileInterface $file, $image_style_name) {
    $errors = [];

    // Check first that the file is an image.
    $image_factory = $this->imageFactory;
    $image = $image_factory->get($file->getFileUri());

    if ($image->isValid()) {
      /** @var \Symfony\Component\Mime\MimeTypeGuesserInterface $extension_mime_type_guesser */
      $extension_mime_type_guesser = \Drupal::service('file.mime_type.guesser.extension');

      $image_style = ImageStyle::load($image_style_name);
      if ($image_style === NULL) {
        $errors[] = $this->t("The image style %image_style does not exist and can not be applied on image upload.", ['%image_style' => $image_style_name]);
      }

      $derived_uri = $file->getFileUri() . '.derived';
      if (!$image_style->createDerivative($file->getFileUri(), $derived_uri)) {
        $errors[] = $this->t("The image style %image_style can not be applied - incorrect style or uploaded image is invalid.", ['%image_style' => $image_style_name]);
      }

      $new_mime = mime_content_type($derived_uri);
      try {
        $this->fileSystem->move($derived_uri, $file->getFileUri(), FileSystemInterface::EXISTS_REPLACE);
      }
      catch (FileException $e) {
        $errors[] = $this->t('The temporary derived image could not be moved to image location.');
      }

      // If image was converted to other format,
      // then change file extension, mimetype.
      if ($new_mime != $extension_mime_type_guesser->guessMimeType($image->getSource())) {
        $file_info = new \SplFileInfo($file->getFilename());
        $extension = $file_info->getExtension();
        $new_extension = str_replace('jpeg', 'jpg', str_replace('image/', '', $new_mime));
        $file->setFilename(preg_replace('"\.' . $extension . '$"', '.' . $new_extension, $file->getFilename()));
        $file->destination = preg_replace('"\.' . $extension . '$"', '.' . $new_extension, $file->destination);
        $file->setMimeType($new_mime);
      }
    }

    return $errors;
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   */
  public function formFieldConfigEditFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $field_config_entity = $form_state->getFormObject()->getEntity();

    if ($field_config_entity->get('field_type') == 'image') {
      $form['upload_image_changer'] = [
        '#type' => 'details',
        '#title' => $this->t('Upload Image Changer'),
        '#open' => TRUE,
        '#weight' => 10,
      ];
      $form['upload_image_changer']['upload_image_style'] = [
        '#title' => $this->t('Style on image upload'),
        '#type' => 'select',
        '#options' => image_style_options(FALSE),
        '#empty_option' => '<' . $this->t('no style') . '>',
        '#default_value' => $field_config_entity->getThirdPartySetting('upload_image_changer', 'upload_image_style'),
        '#description' => $this->t('Apply the image style on an image upload.'),
        '#weight' => 0,
      ];

      $form['#entity_builders'][] = [$this, 'formFieldConfigEditFormBuilder'];
    }
  }

  /**
   * Callback form entity builder.
   */
  public function formFieldConfigEditFormBuilder($entity_type, FieldConfig $entity, &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('upload_image_style')) {
      $entity->setThirdPartySetting('upload_image_changer', 'upload_image_style', $form_state->getValue('upload_image_style'));
      return;
    }

    $entity->unsetThirdPartySetting('upload_image_changer', 'upload_image_style');
  }

}
